import { combineReducers } from "redux";
import counterReducer from "./counterReducer"
import authReducer from "./authReducer";

export const allReducers = combineReducers({
    counterReducer,
    authReducer
})