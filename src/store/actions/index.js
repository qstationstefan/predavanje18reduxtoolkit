export const increment = () => {
    return {
        type: "INCREMENT"
    }
}

export const decrement = () => {
    return {
        type: "DECREMENT"
    }
}

export const incrementN = (n) => {
    return {
        type: "INCREMENT_N",
        payload: n
    }
}