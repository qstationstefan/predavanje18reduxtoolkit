import { useSelector, useDispatch } from "react-redux";
import Counter from "./components/Counter";
import { signIn, signOut } from "./reduxtoolkit/auth-slice";
import { counterActions } from "./reduxtoolkit/counter-slice";

function App() {
  const isLogged = useSelector(state => state.auth.isLogged);
  const dispatch = useDispatch();

  const loginHandler = () => {
    if(isLogged) {
      dispatch(counterActions.resetToInitial());
      dispatch(signOut());
    } else {
      dispatch(signIn())
    }
  }

  return (
    <div>
      <button onClick={loginHandler}>Log in/out</button>
      {isLogged && <Counter />}
    </div>
  );
}

export default App;
