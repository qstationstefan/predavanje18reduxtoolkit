import { useDispatch, useSelector } from "react-redux";
import { counterActions } from "../reduxtoolkit/counter-slice";

const INCREMENT_N = 5;
const Counter = () => {
    const counterValue = useSelector(state => state.counter.counterValue);
    const dispatch = useDispatch();

    const incrementHandler = () => {
        dispatch(counterActions.increment());
    }

    const decrementHandler = () => {
        dispatch(counterActions.decrement());
    }
 
    const incrementNHandler = () => {
        dispatch(counterActions.incrementN(INCREMENT_N));
    }

    return (
        <div>
            <p>Counter value: {counterValue}</p>
            <button onClick={incrementHandler}>Increment</button>
            <button onClick={decrementHandler}>Decrement</button>
            <button onClick={incrementNHandler}>Increment {INCREMENT_N}</button>
        </div>
    )
}

export default Counter;