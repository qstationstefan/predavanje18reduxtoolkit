import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isLogged: false
}

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        signIn(state) {
            state.isLogged = true
        },
        signOut(state) {
            state.isLogged = false
        }

    }
})

export const { signIn, signOut } = authSlice.actions;