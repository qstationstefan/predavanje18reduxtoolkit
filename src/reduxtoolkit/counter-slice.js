import { createSlice } from "@reduxjs/toolkit";

const initialState = { counterValue: 0 }
export const counterSlice = createSlice({
    name: "counter",
    initialState: initialState,
    reducers: {
        increment(state) {
            state.counterValue++;
        },
        decrement(state) {
            state.counterValue--;
        },
        incrementN(state, { payload }) {
            state.counterValue += payload;
        },
        resetToInitial(state) {
            return initialState;
        }
    }
})

export const counterActions = counterSlice.actions;